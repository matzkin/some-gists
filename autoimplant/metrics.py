"""
Calculation of similarity between two binary volumes. Includes:
- Dice Similarity Score (DSC)
- Border Dice Similarity Score (BDSC)
- Hausdorff Distance (HD)
- 95% Hausdorff Distance (HD95)
credits: MedPy http://pydoc.net/MedPy/0.2.2/medpy.metric.binary/
"""
import os.path
from glob import glob

import matplotlib.pyplot as plt
import nrrd
import numpy
import numpy as np
import pandas as pd
import seaborn as sns
from scipy.ndimage import _ni_support
from scipy.ndimage.morphology import distance_transform_edt, binary_erosion, \
    generate_binary_structure
sns.set_style("whitegrid")


def __surface_distances(result, reference, voxelspacing=None, connectivity=1):
    """
    The distances between the surface voxel of binary objects in result and their
    nearest partner surface voxel of a binary object in reference.
    """
    result = numpy.atleast_1d(result.astype(numpy.bool))
    reference = numpy.atleast_1d(reference.astype(numpy.bool))
    if voxelspacing is not None:
        voxelspacing = _ni_support._normalize_sequence(voxelspacing,
                                                       result.ndim)
        voxelspacing = numpy.asarray(voxelspacing, dtype=numpy.float64)
        if not voxelspacing.flags.contiguous:
            voxelspacing = voxelspacing.copy()

    # binary structure
    footprint = generate_binary_structure(result.ndim, connectivity)

    # test for emptiness
    if 0 == numpy.count_nonzero(result):
        raise RuntimeError(
            'The first supplied array does not contain any binary object.')
    if 0 == numpy.count_nonzero(reference):
        raise RuntimeError(
            'The second supplied array does not contain any binary object.')

        # extract only 1-pixel border line of objects
    result_border = result ^ binary_erosion(result, structure=footprint,
                                            iterations=1)
    reference_border = reference ^ binary_erosion(reference,
                                                  structure=footprint,
                                                  iterations=1)

    # compute average surface distance
    # Note: scipys distance transform is calculated only inside the borders of the
    #       foreground objects, therefore the input has to be reversed
    dt = distance_transform_edt(~reference_border, sampling=voxelspacing)
    sds = dt[result_border]

    return sds


def hd(result, reference, voxelspacing=None, connectivity=1):
    """
    Hausdorff Distance.

    Computes the (symmetric) Hausdorff Distance (HD) between the binary objects in two
    images. It is defined as the maximum surface distance between the objects.

    Parameters
    ----------
    result : array_like
        Input data containing objects. Can be any type but will be converted
        into binary: background where 0, object everywhere else.
    reference : array_like
        Input data containing objects. Can be any type but will be converted
        into binary: background where 0, object everywhere else.
    voxelspacing : float or sequence of floats, optional
        The voxelspacing in a distance unit i.e. spacing of elements
        along each dimension. If a sequence, must be of length equal to
        the input rank; if a single number, this is used for all axes. If
        not specified, a grid spacing of unity is implied.
    connectivity : int
        The neighbourhood/connectivity considered when determining the surface
        of the binary objects. This value is passed to
        `scipy.ndimage.morphology.generate_binary_structure` and should usually be :math:`> 1`.
        Note that the connectivity influences the result in the case of the Hausdorff distance.

    Returns
    -------
    hd : float
        The symmetric Hausdorff Distance between the object(s) in ```result``` and the
        object(s) in ```reference```. The distance unit is the same as for the spacing of
        elements along each dimension, which is usually given in mm.

    See also
    --------
    :func:`assd`
    :func:`asd`

    Notes
    -----
    This is a real metric. The binary images can therefore be supplied in any order.
    """
    hd1 = __surface_distances(result, reference, voxelspacing,
                              connectivity).max()
    hd2 = __surface_distances(reference, result, voxelspacing,
                              connectivity).max()
    hd = max(hd1, hd2)
    return hd


def hd95(result, reference, voxelspacing=None, connectivity=1):
    """
    95th percentile of the Hausdorff Distance.

    Computes the 95th percentile of the (symmetric) Hausdorff Distance (HD) between the binary objects in two
    images. Compared to the Hausdorff Distance, this metric is slightly more stable to small outliers and is
    commonly used in Biomedical Segmentation challenges.

    Parameters
    ----------
    result : array_like
        Input data containing objects. Can be any type but will be converted
        into binary: background where 0, object everywhere else.
    reference : array_like
        Input data containing objects. Can be any type but will be converted
        into binary: background where 0, object everywhere else.
    voxelspacing : float or sequence of floats, optional
        The voxelspacing in a distance unit i.e. spacing of elements
        along each dimension. If a sequence, must be of length equal to
        the input rank; if a single number, this is used for all axes. If
        not specified, a grid spacing of unity is implied.
    connectivity : int
        The neighbourhood/connectivity considered when determining the surface
        of the binary objects. This value is passed to
        `scipy.ndimage.morphology.generate_binary_structure` and should usually be :math:`> 1`.
        Note that the connectivity influences the result in the case of the Hausdorff distance.

    Returns
    -------
    hd : float
        The symmetric Hausdorff Distance between the object(s) in ```result``` and the
        object(s) in ```reference```. The distance unit is the same as for the spacing of
        elements along each dimension, which is usually given in mm.

    See also
    --------
    :func:`hd`

    Notes
    -----
    This is a real metric. The binary images can therefore be supplied in any order.
    """
    hd1 = __surface_distances(result, reference, voxelspacing, connectivity)
    hd2 = __surface_distances(reference, result, voxelspacing, connectivity)
    hd95 = numpy.percentile(numpy.hstack((hd1, hd2)), 95)
    return hd95


def dc(input1, input2):
    """
    Dice coefficient

    Computes the Dice coefficient (also known as Sorensen index) between the binary
    objects in two images.

    The metric is defined as


    , where :math:`A` is the first and :math:`B` the second set of samples (here: binary objects).

    Parameters
    ----------
    input1 : array_like
        Input data containing objects. Can be any type but will be converted
        into binary: background where 0, object everywhere else.
    input2 : array_like
        Input data containing objects. Can be any type but will be converted
        into binary: background where 0, object everywhere else.

    Returns
    -------
    dc : float
        The Dice coefficient between the object(s) in ```input1``` and the
        object(s) in ```input2```. It ranges from 0 (no overlap) to 1 (perfect overlap).

    Notes
    -----
    This is a real metric.
    """
    input1 = numpy.atleast_1d(input1.astype(numpy.bool))
    input2 = numpy.atleast_1d(input2.astype(numpy.bool))

    intersection = numpy.count_nonzero(input1 & input2)

    size_i1 = numpy.count_nonzero(input1)
    size_i2 = numpy.count_nonzero(input2)

    try:
        dc = 2. * intersection / float(size_i1 + size_i2)
    except ZeroDivisionError:
        dc = 0.0

    return dc


def bdc(implant_1, implant_2, defective_skull, voxelspacing=None, distance=10):
    """
    Border Dice coefficient

    Measures how well a predicted implant fits around border of defective skull by computing the Dice coefficient
    between predicted and ground-truth implant only for voxels close to the defective skull.

    Parameters
    ----------
    implant_1 : array_like
        Input data containing objects. Can be any type but will be converted
        into binary: background where 0, object everywhere else.
    implant_2 : array_like
        Input data containing objects. Can be any type but will be converted
        into binary: background where 0, object everywhere else.
    defective_skull : array_like
        Input data containing objects. Can be any type but will be converted
        into binary: background where 0, object everywhere else.
    voxelspacing : float or sequence of floats, optional
        The voxelspacing in a distance unit i.e. spacing of elements
        along each dimension. If a sequence, must be of length equal to
        the input rank; if a single number, this is used for all axes. If
        not specified, a grid spacing of unity is implied.
    distance : integer
        Distance in milimeters at which voxels are considered to be "border".

    Returns
    -------
    dc : float
        The Dice coefficient between the border of ```implant_1``` and the border
        of ```implant_2```. It ranges from 0 (no overlap) to 1 (perfect overlap).
    """

    dt = distance_transform_edt(~(defective_skull > 0), sampling=voxelspacing)

    implant_1_masked = implant_1.copy()
    implant_2_masked = implant_2.copy()

    implant_1_masked[dt > distance] = 0
    implant_2_masked[dt > distance] = 0

    return dc(implant_1_masked, implant_2_masked)


def save_metrics(folders_csv, out_folder):
    df = pd.read_csv(folders_csv, delimiter=',')  # Read the csv
    paths = df.replace(np.nan, '', regex=True).values.tolist()
    for pred_dir, gt_dir in paths:
        if not os.path.exists(pred_dir) or not os.path.isdir(pred_dir):
            print(f"The pred_dir folder {pred_dir} does not exist. skipping "
                  f"pair..")
            continue
        if not os.path.exists(gt_dir) or not os.path.isdir(gt_dir):
            print(f"The gt_dir folder {gt_dir} does not exist. skipping "
                  f"pair..")
            continue

        cprfx = os.path.commonprefix([pred_dir[::-1], gt_dir[::-1]])[::-1]
        cprfx = cprfx[1:] if len(cprfx) else os.path.split(pred_dir)[1]

        out_dice = os.path.join(out_folder, cprfx, 'dice.csv')
        out_hd = os.path.join(out_folder, cprfx, 'hd.csv')
        out_hd95 = os.path.join(out_folder, cprfx, 'hd95.csv')

        print(f"predictions: {pred_dir}")
        print(f"ground truth: {gt_dir}")
        stats = [
            {},  # Dice
            {},  # HD
            {},  # HD95
        ]
        os.makedirs(os.path.join(out_folder, cprfx), exist_ok=True)

        pred_list = glob('{}/*.nrrd'.format(pred_dir))
        gt_list = glob('{}/*.nrrd'.format(gt_dir))

        for i in range(len(pred_list)):
            pred_fname = os.path.split(pred_list[i])[1]
            gt_fname = os.path.split(gt_list[i])[1]
            print(f"    pred: {pred_fname}, gt: {gt_fname}", end=' ')

            pred, hp = nrrd.read(pred_list[i])
            gt, hg = nrrd.read(gt_list[i])

            print('hd...', end='')
            hdorff = hd(pred, gt)
            print('hd95...', end='')
            hdorff95 = hd95(pred, gt)
            print('dc...')
            dsc = dc(pred, gt)

            stats[0][pred_fname] = [dsc]
            stats[1][pred_fname] = [hdorff]
            stats[2][pred_fname] = [hdorff95]

        print('saving metrics..')
        print(f'  {out_dice}')
        print(f'  {out_hd}')
        print(f'  {out_hd95}')

        dice_df = pd.DataFrame(stats[0]).T.reset_index()
        dice_df.to_csv(out_dice, header=False, index=False)

        hd_df = pd.DataFrame(stats[1]).T.reset_index()
        hd_df.to_csv(out_hd, header=False, index=False)

        hd95_df = pd.DataFrame(stats[2]).T.reset_index()
        hd95_df.to_csv(out_hd95, header=False, index=False)


def make_plots(folder):
    csv_files = {}
    for root, dirs, files in os.walk(folder):
        # Since there could be subfolder with metrics for each folder, I gather
        # all the same metric files together and append them for the same plot
        for file in files:
            if file.endswith('.csv'):
                full_file_path = os.path.join(root, file)
                if file not in csv_files:
                    csv_files[file] = [full_file_path]
                else:
                    csv_files[file].append(full_file_path)

    # Load the metrics
    metrics = {}
    for metric_name, file_paths in csv_files.items():
        ccat_values = np.zeros(0, dtype=np.float)  # Concatenated metric values
        for file_path in file_paths:
            # Extract the values column (the first one has the filenames)
            val_df = pd.read_csv(file_path, header=None)
            values = np.array(val_df.values.tolist())[:, 1].astype(np.float)
            ccat_values = np.concatenate([ccat_values, values])
        metrics[metric_name] = ccat_values

    # Plot
    # # Initialize the figure with a logarithmic x axis
    # ax.set_xscale("log")
    fig1, axs = plt.subplots(1, len(metrics.keys()))
    for i, (metric_name, vals) in enumerate(metrics.items()):
        axs[i].set_title(metric_name)
        axs[i].boxplot(vals, showmeans=True)
    plt.show()

if __name__ == "__main__":
    # Get the preds,gt paths from the csv and save the metrics in out_fold
    aich21 = '/home/fmatzkin/Code/some-gists/autoimplant/aich21.csv'
    out_fold = '/home/fmatzkin/Code/some-gists/autoimplant/aich21_metrics'
    # save_metrics(aich21, out_fold)

    task1 = '/home/fmatzkin/Code/some-gists/autoimplant/aich21_metrics/task1'
    task3 = '/home/fmatzkin/Code/some-gists/autoimplant/aich21_metrics/task3'
    # make_plots(task1)
    make_plots(task3)

# HOWTO read
# a= pd.read_csv('/home/fmatzkin/Code/some-gists/autoimplant/aich21_metrics/additional_test_set_for_participants/dice.csv', header=None)
# a.values.tolist()
