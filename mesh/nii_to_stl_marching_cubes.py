""" Convert nii.gz images to surfaces.

  This algorithm will take preprocessed images and will run the marching cubes
  algorithm, generating a surface in the pixels with intensity values 1.

  Extracted from https://pyscience.wordpress.com/2014/09/11/surface-extraction-creating-a-mesh-from-pixel-data-using-python-and-vtk/  # noqa
  The images should be previously preprocessed (i.e. a bone segmentation is
  needed as input).
"""
import os.path
import sys
import tempfile

import SimpleITK as sitk
import vtk


def sitk_to_stl_marching_cubes(sitk_img, output_file=None):
    im = tempfile.NamedTemporaryFile(suffix='.nii.gz', delete=False)
    im_path = im.name
    sitk.WriteImage(sitk_img, im_path)
    output_file = nii_to_stl_marching_cubes(im_path, output_file)

    return output_file


def nii_to_stl_marching_cubes(input_im_path, output_file=None):
    # If no output path provided
    alt_out_path = input_im_path.replace("nii.gz", "stl")
    output_file = output_file if output_file else alt_out_path

    reader = vtk.vtkNIFTIImageReader()
    reader.SetFileName(input_im_path)
    reader.Update()

    # Marching Cubes
    dmc = vtk.vtkMarchingCubes()
    dmc.SetInputData(reader.GetOutput())
    dmc.GenerateValues(1, 1, 1)
    dmc.Update()

    transform = vtk.vtkTransform()
    qfm = reader.GetQFormMatrix()
    t1 = qfm.GetElement(0, 0) * qfm.GetElement(0, 3)
    t2 = qfm.GetElement(1, 1) * qfm.GetElement(1, 3)
    t3 = qfm.GetElement(2, 2) * qfm.GetElement(2, 3)
    transform.Translate(t1, t2, t3)

    transformPoly = vtk.vtkTransformPolyDataFilter()
    transformPoly.SetInputConnection(dmc.GetOutputPort())
    transformPoly.SetTransform(transform)
    transformPoly.Update()

    # Save the mesh
    writer = vtk.vtkSTLWriter()
    writer.SetInputConnection(transformPoly.GetOutputPort())
    writer.SetFileTypeToBinary()
    writer.SetFileName(output_file)
    writer.Write()

    return output_file

    # smooth_1(dmc.GetOutput(), out_path=output_file.replace('.stl',
    #                                                        '_sm1.stl'),
    #          transform=transform)
    # smooth_2(dmc.GetOutput(), out_path=output_file.replace('.stl',
    #                                                        '_sm2.stl'),
    #          transform=transform)


def smooth_1(poly, num_iter=15, rlx_factor=0.1, out_path=None, transform=None):
    '''
    http://www.vtk.org/doc/nightly/html/classvtkSmoothPolyDataFilter.html
    '''
    print("smoothing1")
    smoothFilter = vtk.vtkSmoothPolyDataFilter()
    smoothFilter.SetInputData(poly)
    smoothFilter.SetNumberOfIterations(num_iter)
    smoothFilter.SetRelaxationFactor(rlx_factor)
    smoothFilter.FeatureEdgeSmoothingOff()
    # smoothFilter.FeatureEdgeSmoothingOn()
    smoothFilter.BoundarySmoothingOff()
    # smoothFilter.BoundarySmoothingOff()
    smoothFilter.Update()

    writer = vtk.vtkSTLWriter()
    if transform:
        transformPoly = vtk.vtkTransformPolyDataFilter()
        transformPoly.SetInputConnection(smoothFilter.GetOutputPort())
        transformPoly.SetTransform(transform)
        transformPoly.Update()
        writer.SetInputConnection(transformPoly.GetOutputPort())
    else:
        writer.SetInputConnection(smoothFilter.GetOutputPort())
    writer.SetFileTypeToBinary()
    writer.SetFileName(out_path)
    writer.Write()


def smooth_2(poly, iter=15, feature_angle=120, pass_band=0.001,
             out_path=None, transform=None):
    '''
    http://www.vtk.org/doc/nightly/html/classvtkWindowedSincPolyDataFilter.html#details
    '''
    print("smoothing2")
    smoother = vtk.vtkWindowedSincPolyDataFilter()
    smoother.SetInputData(poly)
    smoother.SetNumberOfIterations(iter)
    smoother.BoundarySmoothingOff()
    smoother.FeatureEdgeSmoothingOff()
    smoother.SetFeatureAngle(feature_angle)
    smoother.SetPassBand(pass_band)
    smoother.NonManifoldSmoothingOn()
    smoother.NormalizeCoordinatesOn()
    smoother.Update()

    writer = vtk.vtkSTLWriter()
    if transform:
        transformPoly = vtk.vtkTransformPolyDataFilter()
        transformPoly.SetInputConnection(smoother.GetOutputPort())
        transformPoly.SetTransform(transform)
        transformPoly.Update()
        writer.SetInputConnection(transformPoly.GetOutputPort())
    else:
        writer.SetInputConnection(smoother.GetOutputPort())
    writer.SetFileTypeToBinary()
    writer.SetFileName(out_path)
    writer.Write()


def main_file(input_im_path=None, out_im_path=None):
    not_supported = ['.stl']
    input_im_path = sys.argv[1] if not input_im_path else input_im_path
    ext = os.path.splitext(input_im_path)[1]
    print(f'Input file... {input_im_path}')
    if ext in not_supported:
        print(f'File extension ({ext}) not supported. Skipping..')
        return

    alt_out_path = input_im_path.replace("nii.gz", "stl")
    out_im_path = sys.argv[2] if len(sys.argv) > 2 else out_im_path
    output_file = out_im_path if out_im_path else alt_out_path
    print(f'Saving to {output_file}')
    nii_to_stl_marching_cubes(input_im_path, output_file)


def main_ff(input_ff=None, out_path=None):
    input_ff = sys.argv[1] if len(sys.argv) > 1 else input_ff
    out_path = sys.argv[2] if len(sys.argv) > 2 else out_path
    if os.path.isfile(input_ff) and input_ff.endswith('.nii.gz'):
        main_file(input_ff, out_path)
    elif os.path.isdir(input_ff):
        for file in os.listdir(input_ff):
            fpath = os.path.join(input_ff, file)
            if os.path.isfile(fpath) and fpath.endswith('.nii.gz'):
                main_file(fpath, out_path)


if __name__ == "__main__":
    main_ff(
        '/home/franco/Code/datasets/cq500/converted/selected/test/preprocessed_ct_to_skull/test2')
