msg = """Folderize content by patient.

Given a folder with many images of different patients and types of images, sort
them in folders for each patient. Note that the images for each patient must
differ in the given suffixes.

Example:
- images
    - sub001_ct.nii.gz
    - sub001_seg.nii.gz
    - sub002_ct.nii.gz
    - sub002_seg.nii.gz

running 'python filderize_by_patient.py ~/folder _ct.nii.gz _seg.nii.gz' will
produce:
- images
    - sub001
        - sub001_ct.nii.gz
        - sub001_seg.nii.gz
    - sub002
        - sub002_ct.nii.gz
        - sub002_seg.nii.gz
        
You have to provide at least two arguments: the FOLDER PATH and at least one 
SUFFIX, as follows:
     python filderize_by_patient.py [FOLDER_PATH] [SUFFIX | ES]
"""

import os
import sys
import numpy as np
import shutil


def sort_folders(path, suffs):
    files = os.listdir(path)
    for file in files:
        contains = [file.endswith(s) for s in suffs]
        if not any(contains):
            supp_exts = ", ".join(suffs)
            print(f"The file {file} does not contain any of the provided "
                  f"suffixes ({supp_exts}), so it will not be moved.")
            continue
        suff = suffs[np.where(contains)[0][0]]
        folder_name = file[0:-len(suff)]
        new_folder_path = os.path.join(path, folder_name)
        os.makedirs(new_folder_path, exist_ok=True)
        full_file_path = os.path.join(path, file)
        new_file_path = os.path.join(new_folder_path, file)
        print(f"Moving {full_file_path} to {new_file_path}")
        shutil.move(full_file_path, new_file_path)


def show_help():
    print(msg)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        show_help()
    else:
        path, suffs = sys.argv[1], sys.argv[2:]
        sort_folders(path, suffs)
