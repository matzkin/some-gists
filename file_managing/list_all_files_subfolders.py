""" This script lists the .nii.gz images in the CENTER-TBI dataset.

 Note that the duplicate images (i.e. subfolders with Hour+ and Hour-
 simultaneously present) will not be included.
 """

import csv
import os
import sys

import SimpleITK as sitk


def list_all_files_subfolders():
    folder = sys.argv[1]
    print(f'Input folder... {folder}')

    default_ofile = os.path.join(folder, 'files.csv')
    out_file = sys.argv[2] if len(sys.argv) > 2 else default_ofile
    print(f'Saving into... {out_file}')

    filelist = [('image', 'mask')]
    # filelist = [('image', 'd1', 'd2', 'd3', 's1', 's2', 's3')]
    for root, dirs, files in os.walk(folder):
        spl = root.split('Hour')
        spl_c = spl.copy()
        if len(spl) > 2:
            h_sign = spl[2][0]  # Second 'Hour' substring sign.
            if h_sign == '-':
                spl_c[2] = spl_c[2].replace('-', '+', 1)
                pos_path = 'Hour'.join(spl_c)
                if os.path.exists(pos_path):  # The positive path exists
                    print(f"Skipping: {root}")
                    continue  # Skip the negative path
        # print(f"Folder: {root}")
        for file in files:
            if file.endswith(".nii.gz"):
                print(f"  {file}.. ", end='')
                file_path = os.path.join(root, file)
                img = sitk.ReadImage(file_path)
                d1, d2, d3 = img.GetSize()
                s1, s2, s3 = img.GetSpacing()
                filelist.append((file_path, ''))
                print(f" added.")
        csv_path = out_file
        with open(csv_path, "w") as fp:  # Save all files CSV
            writer = csv.writer(fp, delimiter=",")
            writer.writerows(filelist)


if __name__ == "__main__":
    list_all_files_subfolders()
