""" This script renames the files listed in a csv with the 2nd col value.
 """

import os
import sys
from shutil import copyfile

import pandas as pd


def replace_names_csv():
    helpmsg = """ replace_names_csv.py
    Copy the files listed in a csv to a new path with the 2nd field value
    as filename (it must include the extension).
    
    Usage:
    python replace_names_csv.py [CSV_PATH] [OUT_FOLDER]
    
    CSV_PATH: Path of the csv with the following format:
    ___example.csv___
    image, newname
    /home/user/images/img1.nii.gz,patient1.nii.gz
    /home/user/images/img2.nii.gz,patient2.nii.gz

    OUT_FOLDER: (Optional) if provided, the images will be all saved in this
    folder. If not provided, the images will be saved in the same folder with 
    the new name."""
    if len(sys.argv) == 1:
        print(helpmsg)
        return
    csv_path = sys.argv[1]
    if not os.path.isfile(csv_path):
        print(f"Wrong csv path. {csv_path}")
        return

    out_folder = None if len(sys.argv) < 2 else sys.argv[2]
    if out_folder:
        os.makedirs(out_folder, exist_ok=True)
        print(f"Out folder: {out_folder}")
    else:
        print(f"The images will be saved in the same folder.")

    df = pd.read_csv(csv_path)
    files = df.values.tolist()

    for f_path, new_f_name in files:
        old_path, old_f_name = os.path.split(f_path)
        new_folder = old_path if not out_folder else out_folder
        new_path = os.path.join(new_folder, new_f_name)
        if os.path.exists(old_path):
            copyfile(f_path, new_path)
            print(f"Copied {f_path} in {new_path}.")
        else:
            print(f"can't copy {f_path}. The path doesn't exists.")


if __name__ == "__main__":
    replace_names_csv()
