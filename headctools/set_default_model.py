# This script sets a given ini file as the default model in the default
# workspace.
import os
import shutil
import sys

from ctunet.utilities import set_cfg_params as load_params, veri_folder as vf

DEFAULT_WORKSPACE = '~/headctools'


def set_model():
    """ Set a given model (defined inside a .ini given as argument) as default.

    If provided, a workspace path can be given as second argument.
    """
    if len(sys.argv) < 2:
        raise AttributeError(".ini file not provided")

    ini_path = sys.argv[1]
    wsp = DEFAULT_WORKSPACE if len(sys.argv) < 3 else sys.argv[2]
    wsp = os.path.expanduser(wsp)

    par_dict = load_params(ini_path)
    name = par_dict['name']

    mc, hd = par_dict['model_class'], par_dict['problem_handler']
    run_name = mc + '_' + hd
    model_folder = os.path.join(os.path.expanduser(wsp), run_name, 'model')
    vf(model_folder)

    model_path = os.path.join(model_folder, name + '.pt')
    if not os.path.exists(model_path):
        raise FileNotFoundError(f"Model path not found ({model_path}).")

    defalut_model_path = os.path.join(model_folder, 'default.pt')
    shutil.copy(model_path, defalut_model_path)
    print(f"saved ({defalut_model_path}).")


if __name__ == '__main__':
    set_model()
