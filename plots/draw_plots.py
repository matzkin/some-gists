import seg_metrics.seg_metrics as sg

labels = [0, 1]
gdth_path = '/home/fmatzkin/Code/datasets/SamCook-FAVO/all/selected/renamed/preds/pred_UNetSP/gt'
pred_path = '/home/fmatzkin/Code/datasets/SamCook-FAVO/all/selected/renamed/preds/pred_UNetSP/pred'
csv_file = '/home/fmatzkin/Code/datasets/SamCook-FAVO/all/selected/renamed/preds/pred_UNetSP/metrics.csv'

metrics = sg.write_metrics(labels=labels[1:],  # exclude background
                  gdth_path=gdth_path,
                  pred_path=pred_path,
                  csv_file=csv_file)
print(metrics)