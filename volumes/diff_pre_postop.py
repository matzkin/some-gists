""" This script performs the difference between pre and postoperative images.

 The images should be in a folder and the filenames should differ in
 preop/postop, example: subj001_preop.nii.gz subj001_postop.nii.gz

 If the files have different suffixes, they can be provided as arguments
 """

import os
import sys

import SimpleITK as sitk
import numpy as np


def cast_norm(image):
    arr = sitk.GetArrayFromImage(image)
    image = sitk.Cast((image - arr.min()) / (arr.max() - arr.min()),
                      sitk.sitkInt8)
    return image


def diff_pre_postop(folder_path=None):
    helpmsg = """ diff_pre_postop.py
    This script performs the difference between pre and postoperative images.

     The images should be in a folder and the filenames should differ in
     preop/postop, example: subj001_preop.nii.gz subj001_postop.nii.gz

    If the files have different suffixes, they can be provided as arguments.
    Note that they have to contain the input_im_path extension.

    Usage:
    python diff_pre_postop.py [FOLDER_PATH] [PREOP_SUFF] [POSTOP_SUFF]"""

    if len(sys.argv) == 1 and not folder_path:  # No folder given
        print(helpmsg)
        return
    elif not folder_path:
        folder_path = sys.argv[1]

    if not os.path.isdir(folder_path):
        print(f"Wrong folder path. {folder_path}")
        return

    if len(sys.argv) == 3:
        print("Wrong number of arguments. \n\n", helpmsg)

    preop_suf = sys.argv[2] if len(sys.argv) == 4 else "preop.nii.gz"
    postop_suf = sys.argv[3] if len(sys.argv) == 4 else "postop.nii.gz"

    preop_files = [f for f in os.listdir(folder_path) if f.endswith(preop_suf)]

    print(f"Processing {folder_path}...")
    for preop_img_fname in preop_files:
        print(f"file: {preop_img_fname}.")
        preop_img_path = os.path.join(folder_path, preop_img_fname)
        postop_img_path = preop_img_path.replace(preop_suf, postop_suf)

        if not os.path.isfile(postop_img_path):
            print(f"{postop_img_path} not found.")
            os.rename(preop_img_path, preop_img_path.replace('all/',
                                                             'all/missing/'))

            continue

        preop_sitk = cast_norm(sitk.ReadImage(preop_img_path))
        postop_sitk = cast_norm(sitk.ReadImage(postop_img_path))

        diff_sitk = sitk.And(sitk.Not(postop_sitk), preop_sitk)
        open_sitk = sitk.BinaryMorphologicalOpening(diff_sitk)

        bigcc_sitk = get_biggest_components(open_sitk)

        diff_path = postop_img_path.replace(postop_suf, 'diff.nii.gz')
        open_path = postop_img_path.replace(postop_suf, 'opened.nii.gz')
        bigcc_path = postop_img_path.replace(postop_suf, 'opened_cc.nii.gz')

        sitk.WriteImage(diff_sitk, diff_path)
        sitk.WriteImage(open_sitk, open_path)
        sitk.WriteImage(bigcc_sitk, bigcc_path)
        print(f"saved {diff_path}.")
        print(f"saved {open_path}.")
        print(f"saved {bigcc_path}.")


def sort_dict_by_val(labels: dict, filt: float = 0.):
    """ Sort dict based on the values and remove the smaller items

    It will sort the whole dict and then remove those key-value pairs whose
    value is smaller than filt * the_first_value.
    """
    # Sort descending by value
    labels = {k: v for k, v in sorted(labels.items(),
                                      key=lambda item: item[1],
                                      reverse=True)}
    i = 0
    for i, val in enumerate(labels.values()):  # Determine cut index (i)
        if val < filt * list(labels.values())[0]:
            break

    # Create the dict with the first i elements only
    return {k: v for k, v in zip(list(labels.keys())[:i],
                                 list(labels.values())[:i])}


def get_biggest_components(image, rel=.8):
    """ Retains the biggest components of a mask.

    It obtains the largest connected components, and according to the rel
    parameter, it draws the components that are at least rel % of the size
    of the biggest CC.
    """

    image = sitk.Cast(image, sitk.sitkUInt32)  # Cast to uint32

    connected_component_filter = sitk.ConnectedComponentImageFilter()
    objects = connected_component_filter.Execute(image)

    labels = {}  # Save label id -> size
    # If there is more than one connected component
    if connected_component_filter.GetObjectCount() > 1:
        objects_data = sitk.GetArrayFromImage(objects)

        # Detect the largest connected component
        for i in range(1, connected_component_filter.GetObjectCount() + 1):
            component_data = objects_data[objects_data == i]
            labels[i] = len(component_data.flatten())  # Voxel count

        labels = sort_dict_by_val(labels, filt=rel)
        data_aux = np.zeros(objects_data.shape, dtype=np.uint8)
        for label in labels.keys():
            data_aux[objects_data == label] = 1

        # Save edited image
        output = sitk.GetImageFromArray(data_aux)
        output.SetSpacing(image.GetSpacing())
        output.SetOrigin(image.GetOrigin())
        output.SetDirection(image.GetDirection())
    else:
        output = image

    return output


if __name__ == "__main__":
    diff_pre_postop()
