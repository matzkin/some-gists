from __future__ import print_function

import SimpleITK as sitk
import sys
import os

if len(sys.argv) < 2:
    print("Usage: python dcm_to_nifti.py <input_directory> <output_file>")
    sys.exit(1)
elif len(sys.argv) == 2:
    input_folder = sys.argv[1]
    content = os.listdir(input_folder)
    content = [os.path.join(input_folder, f) for f in content]
    # If the folder contains a dcm file, process only this folder
    if any([f.endswith('.dcm') for f in content]):
        folders = [input_folder]
    elif any([os.path.isdir(f) for f in content]):
        folders = [f for f in content if os.path.isdir(f)]

    ext = '.nii.gz'
    for folder in folders:
        if len(sys.argv) == 3:
            o_f_param = sys.argv[2]
            if len(folders) > 1:
                if os.path.splitext(o_f_param)[1] != '':
                    out_image_path = os.path.join(os.path.split(o_f_param)[0],
                                                  os.path.split(folder)[
                                                      1] + ext)
                else:
                    out_image_path = os.path.join(o_f_param,
                                                  os.path.split(folder)[
                                                      1] + ext)

            elif len(folders) == 1:
                if os.path.splitext(o_f_param)[1] != '':
                    out_image_path = o_f_param
                else:
                    out_image_path = os.path.join(o_f_param,
                                                  os.path.split(folder)[
                                                      1] + ext)
        else:
            out_image_path = os.path.join(os.path.split(folder)[0],
                                          os.path.split(folder)[1] + ext)

        print("Reading Dicom directory:", folder)
        reader = sitk.ImageSeriesReader()

        dicom_names = reader.GetGDCMSeriesFileNames(folder)
        reader.SetFileNames(dicom_names)

        image = reader.Execute()

        size = image.GetSize()
        print("Image size:", size[0], size[1], size[2])

        print("Writing image:", out_image_path)

        sitk.WriteImage(image, out_image_path)
