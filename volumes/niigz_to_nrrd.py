import sys

from nrrd_to_niigz import from_to


def convert(input_ff=None, include_subfolders=False, remove_orig=False):
    helpmsg = """ niigz_to_nrrd.py
    This script grabs a file/folder path and it converts it or its contents 
    to nrrd.

    Usage:
    python niigz_to_nrrd.py [FILE_PATH/FOLDER_PATH]"""

    if len(sys.argv) == 1 and not input_ff:  # No folder given
        print(helpmsg)
        return
    elif not input_ff:
        input_ff = sys.argv[1]
    if len(sys.argv) == 4:
        include_subfolders = sys.argv[2]
        remove_orig = sys.argv[3]
    elif len(sys.argv) == 3:
        include_subfolders = sys.argv[2]

    from_to(input_ff, '.nii.gz', '.nrrd', include_subfolders, remove_orig)


if __name__ == "__main__":
    convert()
