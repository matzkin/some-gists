# Convert .nrrd images to .nii.gz
import os.path
import sys
import SimpleITK as sitk


def from_to(path, ext_from, ext_to, include_subfolders=False,
            remove_orig=False):
    def wr(x):  # Cast image and write
        img = sitk.Cast(sitk.ReadImage(x), sitk.sitkUInt8)
        sitk.WriteImage(img, x.replace(ext_from, ext_to), True)

    if os.path.isdir(path):
        for root, dirs, all_files in os.walk(path):
            files = [f for f in all_files if f.endswith(ext_from)]
            for file in files:
                input_f = os.path.join(root, file)
                print(f"processing {input_f}...")
                wr(input_f)
                if remove_orig:
                    os.remove(input_f)
                    print("original file removed.")
            if not include_subfolders:
                break
    else:
        print(f"processing {path}...")
        wr(path)
    print("done.")


def convert(input_ff=None, include_subfolders=False, remove_orig=False):
    helpmsg = """ nrrd_to_niigz.py
    This script grabs a file/folder path and it converts it or its contents 
    to nii.gz.

    Usage:
    python nrrd_to_niigz.py [FILE_PATH/FOLDER_PATH]"""

    if len(sys.argv) == 1 and not input_ff:  # No folder given
        print(helpmsg)
        return
    elif not input_ff:
        input_ff = sys.argv[1]
    if len(sys.argv) == 4:
        include_subfolders = sys.argv[2]
        remove_orig = sys.argv[3]
    elif len(sys.argv) == 3:
        include_subfolders = sys.argv[2]

    from_to(input_ff, '.nrrd', '.nii.gz', include_subfolders, remove_orig)


if __name__ == "__main__":
    convert()
