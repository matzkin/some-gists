""" This script aligns pre and postoperative imgs.

 The images should be in a folder and the filenames should differ in
 preop/postop, example: subj001_preop.nii.gz subj001_postop.nii.gz

 If the files have different suffixes, they can be provided as arguments
 """

import os
import sys

import ants


def align_pre_postop(folder_path=None):
    helpmsg = """ align_pre_postop.py
    This script aligns pre and postoperative images.

     The images should be in a folder and the filenames should differ in
     preop/postop, example: subj001_preop.nii.gz subj001_postop.nii.gz

    If the files have different suffixes, they can be provided as arguments.
    Note that they have to contain the input_im_path extension.

    Usage:
    python diff_pre_postop.py [FOLDER_PATH] [PREOP_SUFF] [POSTOP_SUFF]"""

    if len(sys.argv) == 1 and not folder_path:  # No folder given
        print(helpmsg)
        return
    elif not folder_path:
        folder_path = sys.argv[1]

    if not os.path.isdir(folder_path):
        print(f"Wrong folder path. {folder_path}")
        return

    if len(sys.argv) == 3:
        print("Wrong number of arguments. \n\n", helpmsg)

    preop_suf = sys.argv[2] if len(sys.argv) == 4 else "preop.nii.gz"
    postop_suf = sys.argv[3] if len(sys.argv) == 4 else "postop.nii.gz"

    preop_files = [f for f in os.listdir(folder_path) if f.endswith(preop_suf)]

    for preop_img_fname in preop_files:
        preop_img_path = os.path.join(folder_path, preop_img_fname)
        postop_img_path = preop_img_path.replace(preop_suf, postop_suf)

        if not os.path.isfile(postop_img_path):
            print(f"{postop_img_path} not found.")
            continue

        print(f"Aligning {preop_img_fname} with its postoperative image.. ",
              end='')

        fixed = ants.image_read(preop_img_path)
        moving = ants.image_read(postop_img_path)
        out_image = postop_img_path.replace(postop_suf, 'aligned' + postop_suf)

        my_tx = ants.registration(fixed=fixed, moving=moving,
                                  type_of_transform="Affine")
        reg_transform = my_tx['fwdtransforms']

        # Since it is a binary image I apply the transform using nearest.
        print("applying nearestNeighbor transform...")
        transf_ct_nn = ants.apply_transforms(fixed=fixed, moving=moving,
                                           transformlist=[reg_transform[0]],
                                           interpolator='nearestNeighbor')

        ants.image_write(transf_ct_nn, out_image)


if __name__ == "__main__":
    align_pre_postop()
